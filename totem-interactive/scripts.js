var stage, w, h, loader, pipe1height, pipe2height, pipe3height, startX, startY, wiggleDelta;
var background, bird, ground, pipe, bottomPipe, pipes, rotationDelta, counter, counterOutline, metro,city, buildings, people, cloud, cloud2, cloud3, cloud4;
var stationName;
var started = false; 
var startJump = false; // Has the jump started?

var jumpAmount = 80; // How high is the jump?
var jumpTime = 400;

var dead = false; // is the bird dead?
var KEYCODE_SPACE = 32;     //usefull keycode 
var gap = 430;
var masterPipeDelay = 150; // delay between pipes
var pipeDelay = masterPipeDelay; //counter used to monitor delay

var counterShow = false;
var stationNameChanged = false;
var metroCounter = 0;
var DifficultyLevel = 300;

var stations = [
                "Radlická",
                "Smíchovské nádraží",
                "Anděl",
                "Karlovo náměstí",
                "Národní třída",
                "Můstek",
                "Náměstí Republiky",
                "Florenc",
                "Křižíkova",
                "Invalidovna",
                "Palmovka"
            ]

document.onkeydown = handleKeyDown;

function init() {
    if (window.top != window) {
        //document.getElementById("header").style.display = "none";
    }


    // createjs.MotionGuidePlugin.install();

    stage = new createjs.Stage("testCanvas");

    createjs.Touch.enable(stage);
    // stage.canvas.width = document.body.clientWidth; //document.width is obsolete
    // stage.canvas.height = document.body.clientHeight; //document.height is obsolete

    // grab canvas width and height for later calculations:
    w = stage.canvas.width;
    h = stage.canvas.height;

    manifest = [
        {src:"./img/mravenecnik.png", id:"bird"},
        {src:"./img/ladybug.png", id:"ladybug"},
        {src:"./img/dragonfly.png", id: "dragonfly"},
        {src:"./img/peanut-butterfly.png", id: "butterfly"},
        {src:"./img/background.png", id:"background"},
        {src:"./img/only_ground.png", id:"ground"},
        {src:"./img/chimney.png", id:"pipe"},
        {src:"http://www.appcycle.me/flappy/img/restart.png", id:"start"},
        {src:"http://www.appcycle.me/flappy/img/share.png", id:"share"},
        {src:"./img/metro.png", id: "metro"},
        {src:"./img/city.png", id: "city"},
        {src: "./img/only_city.png", id:"buildings"},
        {src: "./img/people.png", id: "people"},
        {src: "./img/cloud.png", id: "cloud"},
        {src: "./img/logo.png", id: "cloud2"}

    ];

    loader = new createjs.LoadQueue(false);
    loader.addEventListener("complete", handleComplete);
    loader.loadManifest(manifest);
}

function handleComplete() {

    background = new createjs.Shape();
    background.graphics.beginBitmapFill(loader.getResult("background")).drawRect(0,0,w,h);

    var groundImg = loader.getResult("ground");
    ground = new createjs.Shape();
    ground.graphics.beginBitmapFill(groundImg).drawRect(0, 0, w+groundImg.width, groundImg.height);
    ground.tileW = groundImg.width;
    ground.y = h-groundImg.height;

    var metroImg = loader.getResult("metro");
    metro = new createjs.Shape();
    metro.graphics.beginBitmapFill(metroImg).drawRect(metroImg.width,0, metroImg.width, metroImg.height);
    metro.tileW = metroImg.width;
    metro.y = h-2.6*metroImg.height;

    var buildingsImg = loader.getResult("buildings");
    buildings = new createjs.Shape();
    buildings.graphics.beginBitmapFill(buildingsImg).drawRect(0,0, w+buildingsImg.width, buildingsImg.height);
    buildings.tileW = buildingsImg.width;
    buildings.y = h - groundImg.height - buildingsImg.height;





/*
var buildingsImg = loader.getResult("buildings");
    buildings = new createjs.Shape();
    buildings.graphics.beginBitmapFill(buildingsImg).drawRect(0,0, buildingsImg.width, buildingsImg.height);
    buildings.tileW = buildingsImg.width;
    buildings.y = h - groundImg.height - buildingsImg.height;
    buildings.x = 20;
*/




    var cityImg = loader.getResult("city");
    city = new createjs.Shape();
    city.graphics.beginBitmapFill(cityImg).drawRect(0, 0, w+cityImg.width, cityImg.height);
    city.tileW = cityImg.width;
    city.y = h - groundImg.height - cityImg.height;

    var peopleImg = loader.getResult("people");
    people = new createjs.Shape();
    people.graphics.beginBitmapFill(peopleImg).drawRect(0,0,w+peopleImg.width, peopleImg.height);
    people.tileW = peopleImg.width;
    people.y = h-peopleImg.height-0.3*peopleImg.height;

    var cloudImg = loader.getResult("cloud");
    cloud = new createjs.Shape();
    cloud.graphics.beginBitmapFill(cloudImg).drawRect(0,0,cloudImg.width, cloudImg.height);
    cloud.tileW = cloudImg.width;
    cloud.y  = cloudImg.height;

    cloud2 = new createjs.Shape();
    cloud2.graphics.beginBitmapFill(cloudImg).drawRect(0,0,cloudImg.width, cloudImg.height);
    cloud2.tileW = cloudImg.width;
    cloud2.y  = 2*cloudImg.height;
    cloud2.x = 1.5*w;

    cloud3 = new createjs.Shape();
    cloud3.graphics.beginBitmapFill(cloudImg).drawRect(0,0,cloudImg.width, cloudImg.height);
    cloud3.tileW = cloudImg.width;
    cloud3.y  = 1.5*cloudImg.height;
    cloud3.x = w;

    var cloudImg2 = loader.getResult("cloud2");
    cloud4 = new createjs.Shape();
    cloud4.graphics.beginBitmapFill(cloudImg2).drawRect(0,0,cloudImg2.width, cloudImg2.height);
    cloud4.tileW = cloudImg2.width;
    cloud4.y  = 1.1*cloudImg2.height;

    


    var data = [];
    data.push(new createjs.SpriteSheet({
        "images": [loader.getResult("bird")],
        "frames": {"width": 160, "height": 147, "count": 8, "regY": 75, "regX": 80}, 
        "animations": {"fly": [0, 7, "fly"], "dive": [1, 1, "dive", 1]}
    }));
    data.push(new createjs.SpriteSheet({
        "images": [loader.getResult("ladybug")],
        "frames": {"width": 186.9, "height": 179, "count": 8, "regY": 75, "regX": 80}, 
        "animations": {"fly": [0, 7, "fly"], "dive": [1, 1, "dive", 1]}
    }));
    data.push(new createjs.SpriteSheet({
        "images": [loader.getResult("dragonfly")],
        "frames": {"width": 187.1, "height": 171, "count": 8, "regY": 75, "regX": 80}, 
        "animations": {"fly": [0, 7, "fly"], "dive": [1, 1, "dive", 1]}
    }));
    data.push(new createjs.SpriteSheet({
        "images": [loader.getResult("butterfly")],
        "frames": {"width": 186.9, "height": 163, "count": 8, "regY": 75, "regX": 80}, 
        "animations": {"fly": [0, 7, "fly"], "dive": [1, 1, "dive", 1]}
    }));
    

    bird = new createjs.Sprite(data[Math.round(Math.random()*10%4)], "fly");

    startX = (w/2) - (92/2);
    startY = 512;
    wiggleDelta = 18;

    // Set initial position and scale 1 to 1
    bird.setTransform(startX, startY, 1, 1);
    // Set framerate
    bird.framerate = 30;

    //338, 512
    // Use a tween to wiggle the bird up and down using a sineInOut Ease
    createjs.Tween.get(bird, {loop:true}).to({y:startY + wiggleDelta}, 380, createjs.Ease.sineInOut).to({y:startY}, 380, createjs.Ease.sineInOut);

    stage.addChild(background);

    pipes = new createjs.Container(); 

    stationName = new createjs.Text(stations[0], "70px 'Arial'", "#ffffff");
    stationName.x = w/2 ;
    stationName.y = h - peopleImg.height*2;
    stationName.textAlign = "center";
    stationName.alpha = 1;
    

    stage.addChild(metro, city, buildings,  bird, cloud, cloud2, cloud3, cloud4);
    stage.addChild(pipes);
    stage.addChild(ground,stationName, metro, people);
    stage.addEventListener("stagemousedown", handleJumpStart);

    counter = new createjs.Text(0, "86px 'Arial'", "#ffffff");
    counterOutline = new createjs.Text(0, "86px 'Arial'", "#666666");
    counterOutline.outline = 5;
    counterOutline.textAlign = 'center';
    counter.textAlign = 'center';
    counterOutline.x = w/2;
    counterOutline.y = 150;
    counter.x = w/2;
    counter.y = 150;
    counter.alpha = 1;
    counterOutline.alpha = 1;
    stage.addChild(counter, counterOutline);

    createjs.Ticker.timingMode = createjs.Ticker.RAF;
    createjs.Ticker.addEventListener("tick", tick);






}

function handleKeyDown(e) {
    //cross browser issues exist
    if(!e){ var e = window.event; }
    switch(e.keyCode) {
    case KEYCODE_SPACE: handleJumpStart();
    }
}

function handleJumpStart() {
    if (!dead) {
        createjs.Tween.removeTweens ( bird );
        bird.gotoAndPlay("jump");
        startJump = true;
        if (!started) {
            started = true;
            counterShow = true; 
        }
    }
}

function diveBird() {
    bird.gotoAndPlay("dive");
}

function restart() {
    //hide anything on stage and show the score
    pipes.removeAllChildren();
    createjs.Tween.get(start).to({y:start.y + 10}, 50).call(removeStart);
    counter.text = 0;
    DifficultyLevel = 300;
    counterOutline.text = 0;
    counterOutline.alpha = 0;
    counter.alpha = 0;
    counterShow = false;
    pipeDelay = masterPipeDelay;
    dead = false;
    started = false;
    startJump = false;
    createjs.Tween.removeTweens ( bird );
    bird.x = startX;
    bird.y = startY;
    bird.rotation = 0;
    createjs.Tween.get(bird, {loop:true}).to({y:startY + wiggleDelta}, 380, createjs.Ease.sineInOut).to({y:startY}, 380, createjs.Ease.sineInOut);
}

function emitDieEvent() {
    var event = document.createEvent('Event');
    event.initEvent('game-over', true, false);
    addGameOverListener();
    window.dispatchEvent(event);
}

function addGameOverListener() {
    window.addEventListener('game-over', function() {
        console.log("DIED");
    });
}

function die() {
    emitDieEvent();
    dead = true;
    bird.gotoAndPlay("dive");
    createjs.Tween.removeTweens ( bird );
    createjs.Tween.get(bird).wait(0).to({y:bird.y + 200, rotation: 90}, (380)/1.5, createjs.Ease.linear) //rotate back
        .call(diveBird) // change bird to diving position
        .to({y:ground.y - 30}, (h - (bird.y+200))/1.5, createjs.Ease.linear); //drop to the bedrock
    createjs.Tween.get(stage).to({alpha:0}, 100).to({alpha:1}, 100);
    start = new createjs.Bitmap(loader.getResult("start"));
    start.alpha = 0;
    start.x = w/2 - start.image.width/2;
    start.y = h/2 - start.image.height/2 - 150;
    share = new createjs.Bitmap(loader.getResult("share"));
    share.alpha = 0;
    share.x = w/2 - share.image.width/2;
    share.y = h/2 - share.image.height/2 - 50;

    // stage.addChild(start)
    // stage.addChild(share)
    // createjs.Tween.get(start).to({alpha:1, y: start.y + 50}, 400, createjs.Ease.sineIn).call(addClickToStart)
    // createjs.Tween.get(share).to({alpha:1, y: share.y + 50}, 400, createjs.Ease.sineIn).call(addClickToStart)

}
function removeStart() {
    stage.removeChild(start);
    stage.removeChild(share);
}
function addClickToStart() {
    start.addEventListener("click", restart);
    share.addEventListener("click", goShare);
}

function goShare() {
    var countText;
    if (counter.text == 1) {
        countText = "1 point";
    } else {
        countText = counter.text + " points";
    }
    window.open("https://twitter.com/share?url=http%3A%2F%2Fappcycle.me/flappy&text=I scored " + countText +  " on HTML5 Flappy Bird.");
}

function tick(event) {
    var deltaS = event.delta/1000;

    var l = pipes.getNumChildren();

    if (bird.y > (ground.y - 40)) {
        if (!dead) {
            die()
        }
        if (bird.y > (ground.y - 30)) {
            createjs.Tween.removeTweens ( bird );
        }
    }

        metro.x = (metro.x - deltaS*600);
        if (metro.x < -2*metro.tileW) {
            metro.x = w+Math.random()*5*w;
            stationNameChanged = false;

        }

        if (metro.x<-metro.tileW && !stationNameChanged) {
            metroCounter++;

            metroCounter = metroCounter%stations.length;
            stationNameChanged = true;
            
            stationName.text = stations[metroCounter];

            
        }
        cloud.x = (cloud.x - deltaS*50);
        cloud2.x = (cloud2.x - deltaS*50);
        cloud3.x = (cloud3.x - deltaS*50);
        cloud4.x = (cloud4.x - deltaS*123);
        if (cloud.x < -w) {
            cloud.x = w;
        }
        if (cloud2.x < -w) {
            cloud2.x = 1.5*w +Math.random()*w;
        }
        if (cloud3.x < -w) {
            cloud3.x = w+Math.random()*w;
        }
        if (cloud4.x < -w) {
            cloud4.x = w;
        }
    if (!dead) {
        // ground.x = (ground.x-deltaS*300) % ground.tileW;
        
        city.x = (city.x - deltaS*20) % city.tileW;
        buildings.x = (buildings.x - deltaS*60) % buildings.tileW;
        people.x = (people.x - deltaS*100) % people.tileW;
        ground.x = (ground.x - deltaS*100) % ground.tileW;
    }


    if (started && !dead) {
        if (pipeDelay == 0) {

            pipe = new createjs.Bitmap(loader.getResult("pipe"));
            pipe.x = w+600;
            pipe.y = (ground.y - 2*gap) * Math.random() + gap*1.5;
            pipes.addChild(pipe);
            // createjs.Tween.get(pipe).to({x:0 - pipe.image.width}, 5100)

            pipe2 = new createjs.Bitmap(loader.getResult("pipe"));
            pipe2.scaleX = -1;
            pipe2.rotation = 180;
            pipe2.x = pipe.x; //+ pipe.image.width
            pipe2.y = pipe.y - gap;
            // createjs.Tween.get(pipe2).to({x:0 - pipe.image.width}, 5100)

            pipes.addChild(pipe2);

            pipeDelay = masterPipeDelay;

        } else {
            pipeDelay = pipeDelay - 1;
        }
        for(var i = 0; i < l; i++) {
            pipe = pipes.getChildAt(i);
            if (pipe) {
                if (true) { // tried replacing true with this, but it's off: pipe.x < bird.x + 92 && pipe.x > bird.x 
                    var collision = ndgmr.checkRectCollision(pipe,bird,1,true);
                    if (collision) {
                        if (collision.width > 8 && collision.height > 8) {
                            die();
                        }
                    }
                }
                pipe.x = (pipe.x - deltaS*DifficultyLevel);
                if (pipe.x <= 338 && pipe.rotation == 0 && pipe.name != "counted") {
                    pipe.name = "counted"; //using the pipe name to count pipes
                    counter.text = counter.text + 1;
                    DifficultyLevel = DifficultyLevel + 15;
                    pipe.x = (pipe.x - deltaS*DifficultyLevel);
                    counterOutline.text = counterOutline.text + 1;
                }
                if (pipe.x + pipe.image.width <= -pipe.w) { 
                    pipes.removeChild(pipe);
                }
            }
        }
        if (counterShow) {
            counter.alpha = 1;
            counterOutline.alpha = 1;
            counterShow = false;
        }
    }



    if (startJump == true) {
        startJump = false;
        bird.framerate = 60;
        bird.gotoAndPlay("fly");
        if (bird.roation < 0) {
            rotationDelta = (-bird.rotation - 20)/5;
        } else {
            rotationDelta = (bird.rotation + 20)/5;
        }
        if (bird.y < -200) {
            bird.y = -200;
        }
        createjs
            .Tween
            .get(bird)
            .to({y:bird.y - rotationDelta, rotation: -20}, rotationDelta, createjs.Ease.linear) //rotate to jump position and jump bird
            .to({y:bird.y - jumpAmount, rotation: -20}, jumpTime - rotationDelta, createjs.Ease.quadOut) //rotate to jump position and jump bird
            .to({y:bird.y}, jumpTime, createjs.Ease.quadIn) //reverse jump for smooth arch
            .to({y:bird.y + 200, rotation: 90}, (380)/1.5, createjs.Ease.linear) //rotate back
            .call(diveBird) // change bird to diving position
            .to({y:ground.y - 30}, (h - (bird.y+200))/1.5, createjs.Ease.linear); //drop to the bedrock
    }


    stage.update(event);
}
